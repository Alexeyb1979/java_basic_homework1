package ua.com.dan_it.fs;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class HomeWork1Numbers {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Input your name: ");
        String usersName = in.nextLine();

        System.out.println("Let game begin!");
        Random random = new Random();
        int randomNum = random.nextInt(101);
        StringBuilder usersNumbers = new StringBuilder();
        boolean result = false;

        while (result != true) {
            System.out.println(randomNum);

            System.out.print("Input number from 0 to 100: ");
            int usersNum = in.nextInt();
            usersNumbers.append(usersNum + ", ");
            if (usersNum < randomNum) {

                System.out.println("Your number is too small. Please, try again.");
            }
            if (usersNum > randomNum) {
                System.out.println("Your number is too big. Please, try again.");
            }
            if (usersNum == randomNum) {
                System.out.println("Congratulations! " + usersName + ".");
                result = true;
            }
        }
        String usersNumbersString = usersNumbers.toString();

        String strArr[] = usersNumbersString.split(", ");
        int[] numUsersArr = new int[strArr.length];
        for (int i = 0; i < strArr.length; i++) {
            numUsersArr[i] = Integer.parseInt(strArr[i]);
        }

        Arrays.sort(numUsersArr);
        System.out.println("You enter numbers: " + Arrays.toString(numUsersArr));
    }
}
